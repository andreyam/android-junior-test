package com.andreyam.geotask;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.andreyam.geotask.R.id.map;

public class ResultActivity extends FragmentActivity implements OnMapReadyCallback {

    MaterialDialog progress_dialog;
    private GoogleMap mMap;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private static final String KEY_ORIGIN_LAT_LNG = "origin_lat_lng";
    private static final String KEY_DEST_LAT_LNG = "dest_lat_lng";
    private static final String KEY_LOCATION = "location";

    private static final String REQUEST = "https://maps.googleapis.com/maps/api/directions/json?origin=";

    Location user_location = null;
    LatLng originLatLng, destLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        if (savedInstanceState != null) {
            user_location = savedInstanceState.getParcelable(KEY_LOCATION);
            originLatLng = savedInstanceState.getParcelable(KEY_ORIGIN_LAT_LNG);
            destLatLng = savedInstanceState.getParcelable(KEY_DEST_LAT_LNG);
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        enableLocation();
    }

    /*
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableLocation() {
        int permission_status = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission_status != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
            // Permission to access the location is missing.
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
            FusedLocationProviderClient mFusedLocationClient =
                    LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            setMapView(location);
                        }
                    });

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (permissions.length == 1 && permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableLocation();
            } else {
                // Permission was denied. Display an dialog.
                new MaterialDialog.Builder(this)
                        .title(R.string.dialog_permissions_title)
                        .content(R.string.dialog_permissions_message)
                        .cancelable(false)
                        .positiveText(R.string.dialog_btn_ok)
                        .negativeText(R.string.dialog_btn_cancel)
                        .neutralText(R.string.dialog_settings)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                ActivityCompat.requestPermissions(ResultActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        LOCATION_PERMISSION_REQUEST_CODE);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                finish();
                            }
                        })
                        .onNeutral(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                openApplicationSettings();
                            }
                        }).build().show();
            }
        }
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, LOCATION_PERMISSION_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            enableLocation();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setMapView(Location location) {

        /*MarkerOptions startPoint, finishPoint;

        startPoint = getIntent().getExtras().getParcelable("START_POINT");
        finishPoint = getIntent().getExtras().getParcelable("FINISH_POINT");

        mMap.addMarker(startPoint);
        mMap.addMarker(finishPoint);*/

        /*if ((startPoint != null)&(finishPoint != null)) {
           mMap.addPolyline(new PolylineOptions()
                    .add(startPoint.getPosition())
                    .add(finishPoint.getPosition()));
        }*/

        if (user_location == null) {    // default run

            // send request
            String ORIGIN = getIntent().getStringExtra("ORIGIN");
            String DESTINATION = getIntent().getStringExtra("DESTINATION");

            ORIGIN = ORIGIN.replace(" ", "+");
            DESTINATION = DESTINATION.replace(" ", "+");
            String API_KEY = getResources().getString(R.string.google_maps_directions_api_key);
            String HTTPS_REQUEST = REQUEST + ORIGIN + "&destination=" + DESTINATION + "&key=" + API_KEY;

            new getDirection().execute(HTTPS_REQUEST);

            if (location != null) {
                user_location = location;
            } else {
                Toast.makeText(ResultActivity.this, R.string.cant_get_location_msg, Toast.LENGTH_SHORT).show();
                user_location = null;
            }

        } else {    // after device rotation

            // set Results
            setResultView(originLatLng, destLatLng);
        }

    }

    private class getDirection extends AsyncTask<String, String, LatLng[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_dialog = new MaterialDialog.Builder(ResultActivity.this)
                    .title(R.string.dlg_wait_title)
                    .content(R.string.dlg_wait_msg)
                    .progress(true, 0)
                    .cancelable(false)
                    .build();
            progress_dialog.show();

        }

        protected LatLng[] doInBackground(String... stringUrl) {

            StringBuilder response = new StringBuilder();
            LatLng[] result = new LatLng[2];
            result[0] = null;
            result[1] = null;
            try {
                URL url = new URL(stringUrl[0]);
                HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(
                            new InputStreamReader(httpConn.getInputStream()), 8192);
                    String strLine;

                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    input.close();
                } else {
                    failureMsg();
                }

                String jsonOutput = response.toString();

                JSONObject jsonObject = new JSONObject(jsonOutput);

                String STATUS_CODE = jsonObject.getString("status");

                if (STATUS_CODE.equals("OK")) {
                    JSONObject route_0 = jsonObject.getJSONArray("routes").getJSONObject(0);
                    JSONObject leg_0 = route_0.getJSONArray("legs").getJSONObject(0);

                    JSONObject start_location = leg_0.getJSONObject("start_location");
                    Double start_lat = start_location.getDouble("lat");
                    Double start_lng = start_location.getDouble("lng");

                    JSONObject end_location = leg_0.getJSONObject("end_location");
                    Double finish_lat = end_location.getDouble("lat");
                    Double finish_lng = end_location.getDouble("lng");

                    result[0] = new LatLng(start_lat, start_lng);
                    result[1] = new LatLng(finish_lat, finish_lng);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(LatLng[] latLng) {

            progress_dialog.dismiss();

            if ((latLng[0] != null)&(latLng[0] != null)) {

                // save Locations for device rotation processing
                originLatLng = latLng[0]; destLatLng = latLng[1];

                setResultView(latLng[0], latLng[1]);

            } else {
                failureMsg();
            }
        }
    }

    private void setResultView(LatLng origLatLng, LatLng destLatLng) {

        // add markers
        mMap.addMarker(new MarkerOptions().position(origLatLng).title(getString(R.string.start_point_title)));
        mMap.addMarker(new MarkerOptions().position(destLatLng).title(getString(R.string.finish_point_title)));

        // add polyline
        mMap.addPolyline(new PolylineOptions().geodesic(false).add(origLatLng).add(destLatLng));

        // success msg
        TextView resMsg = (TextView) findViewById(R.id.res_msg);
        resMsg.setText(R.string.res_success_msg);

        // zoom
        LatLngBounds bounds;
        if (user_location != null) {
            LatLng user_coordinates = new LatLng(user_location.getLatitude(), user_location.getLongitude());
            bounds = LatLngBounds.builder()
                    .include(origLatLng)
                    .include(destLatLng)
                    .include(user_coordinates)
                    .build();
        } else {
            bounds = LatLngBounds.builder()
                    .include(origLatLng)
                    .include(destLatLng)
                    .build();
        }

        //mMap.moveCamera(CameraUpdateFactory.newLatLng(bounds.getCenter()));

                /*mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds,
                        getResources().getDimensionPixelSize(R.dimen.map_bounds_margin)));*/

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,
                getResources().getDimensionPixelSize(R.dimen.map_bounds_margin)));

    }

    /*private Toast failureMsg() {
        return Toast.makeText(this, "Не удалось получить данные маршрута", Toast.LENGTH_SHORT);
    }*/

    private void failureMsg() {
        TextView resMsg = (TextView) findViewById(R.id.res_msg);
        resMsg.setText(R.string.res_failure_msg);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_ORIGIN_LAT_LNG, originLatLng);
            outState.putParcelable(KEY_DEST_LAT_LNG, destLatLng);
            outState.putParcelable(KEY_LOCATION, user_location);
            super.onSaveInstanceState(outState);
        }
    }
}
