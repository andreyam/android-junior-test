package com.andreyam.geotask;

import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

public class MainActivity extends AppCompatActivity implements MainFragment.OnFragmentInteractionListener{

    private ViewPager mViewPager;
    private FloatingActionButton fab;

    private GoogleMap mMap;

    private MarkerOptions startPoint = null, finishPoint = null;
    String origin, destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();

        getSupportFragmentManager().beginTransaction().add(R.id.map_container, mapFragment).commit();

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(52.431218, 30.992690))); // Гомель
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        IconicsDrawable search = new IconicsDrawable(MainActivity.this,
                GoogleMaterial.Icon.gmd_search).sizeDp(16).paddingDp(1)
                .color(ContextCompat.getColor(MainActivity.this, R.color.white));
        fab.setImageDrawable(search);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onListInteraction(Address address) {

        mMap.clear();

        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

        if (mViewPager.getCurrentItem() == 0) {
            startPoint = new MarkerOptions().position(latLng).title(getString(R.string.start_point_title));
            mMap.addMarker(startPoint);
            origin = address.getAddressLine(0) + "+" + address.getAddressLine(1);
        } else {
            finishPoint = new MarkerOptions().position(latLng).title(getString(R.string.finish_point_title));
            mMap.addMarker(finishPoint);
            destination = address.getAddressLine(0) + "+" + address.getAddressLine(1);
            showPathButton();
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

    }

    private void showPathButton() {

        if (fab.getVisibility() == View.VISIBLE) {
            return;
        }

        Animation show_fab = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fab_jump_up);
        fab.startAnimation(show_fab);
    }

    public void searchPathClick(View view) {

        /*if ((startPoint != null) & (finishPoint != null)) {
            Intent path = new Intent(MainActivity.this, ResultActivity.class);
            path.putExtra("START_POINT", startPoint);
            path.putExtra("FINISH_POINT", finishPoint);
            startActivity(path);
        } else {
            Toast.makeText(MainActivity.this, "Выберите начальную и конечную точки пути", Toast.LENGTH_SHORT).show();
        }*/

        if ((startPoint != null) & (finishPoint != null)) {
            Intent path = new Intent(MainActivity.this, ResultActivity.class);
            path.putExtra("ORIGIN", origin);
            path.putExtra("DESTINATION", destination);
            startActivity(path);
            finish();
        } else {
            Toast.makeText(MainActivity.this, R.string.points_selection_msg, Toast.LENGTH_SHORT).show();
        }
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return MainFragment.newInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.section_1_title).toUpperCase();
                case 1:
                    return getString(R.string.section_2_title).toUpperCase();
            }
            return null;
        }
    }
}
