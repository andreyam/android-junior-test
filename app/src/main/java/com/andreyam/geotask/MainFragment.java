package com.andreyam.geotask;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private EditText etSearch;
    private ListView foundedLocations;
    private List<Address> addressList;
    ProgressBar wait;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        //Bundle args = new Bundle();
        //fragment.setArguments(args);
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        wait = (ProgressBar) rootView.findViewById(R.id.wait_progress);
        etSearch = (EditText) rootView.findViewById(R.id.et_search);
        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setFocusableInTouchMode(true);
                etSearch.requestFocus();
                showSoftKeys();
            }
        });
        IconicsDrawable search = new IconicsDrawable(getActivity(),
                GoogleMaterial.Icon.gmd_location_searching).sizeDp(24)
                .color(ContextCompat.getColor(getActivity(), R.color.grey_600));
        ImageButton button = (ImageButton) rootView.findViewById(R.id.btn_search);
        button.setImageDrawable(search);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location = etSearch.getText().toString();
                if (!location.isEmpty()) {
                    wait.setVisibility(View.VISIBLE);
                    hideSoftKeys();
                    setFoundedLocations(location);
                } else {
                    Toast.makeText(getActivity(), R.string.input_seach_value_msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
        foundedLocations = (ListView) rootView.findViewById(R.id.location_list);

        return rootView;
    }

    private void setFoundedLocations(String location){

        Geocoder geocoder = new Geocoder(getActivity());
        try {
            addressList = geocoder.getFromLocationName(location, 7);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            int size = addressList.size();
            if (size != 0) {

                ArrayList<Map<String, Object>> data = new ArrayList<>(size);
                Map<String, Object> map;
                for (int i = 0; i < size; i++) {
                    map = new HashMap<>();
                    Address address = addressList.get(i);
                    map.put("ADDRESS_LINE_0", address.getAddressLine(0));
                    map.put("ADDRESS_LINE_1", address.getAddressLine(1));
                    data.add(map);
                }

                String[] from = { "ADDRESS_LINE_0", "ADDRESS_LINE_1" };
                int[] to = new int[] { R.id.tv_line_0, R.id.tv_line_1 };
                SimpleAdapter sAdapter = new SimpleAdapter(getActivity(), data, R.layout.location_list_item, from, to);
                foundedLocations.setAdapter(sAdapter);
                foundedLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        onListItemPressed(addressList.get(position));
                    }
                });

                wait.setVisibility(View.INVISIBLE);
            } else {
                Toast.makeText(getActivity(), R.string.nothing_found, Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onListItemPressed(Address address) {
        if (mListener != null) {
            mListener.onListInteraction(address);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    interface OnFragmentInteractionListener {
        void onListInteraction(Address address);
    }

    private void showSoftKeys(){
        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService
                        (Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(etSearch, InputMethodManager.SHOW_FORCED);

    }

    private void hideSoftKeys(){
        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService
                        (Activity.INPUT_METHOD_SERVICE);
        try {
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
